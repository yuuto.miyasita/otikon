﻿using UnityEngine;
using UnityEngine.UI;

public class InputFieldAction : MonoBehaviour
{
    private InputField _inputField;

    /// <summary>
    /// Start
    /// </summary>
    private void Start()
    {
        _inputField = GetComponent<InputField>();
    }

    /// <summary>
    /// InputFieldの編集終了時
    /// </summary>
    public void OnEditComplete()
    {
        Debug.Log(_inputField.text);
    }
}