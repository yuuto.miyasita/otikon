﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TimeScript : MonoBehaviour
{
    private float time = 60;
    public ball BallScript;
    public GameObject exchangeButton;
    public GameObject gameOverText;

    void Start()
    {
        gameOverText.SetActive(false);
        //初期値60を表示
        //float型からint型へCastし、String型に変換して表示
        GetComponent<Text>().text = ((int)time).ToString();
    }

    void Update()
    {
        //1秒に1ずつ減らしていく
        time -= Time.deltaTime;
        if(time< 0)
        {
            StartCoroutine("GameOver");
        }
        //マイナスは表示しない
        if (time < 0) time = 0;
        GetComponent<Text>().text = ((int)time).ToString();
    }
    IEnumerator GameOver()
    {
        gameOverText.SetActive(true);
        exchangeButton.GetComponent<Button>().interactable = false;
        BallScript.isPlaying = false;
        yield return new WaitForSeconds(2.0f);
        if (Input.GetMouseButtonDown(0))
        {
            Application.LoadLevel("title");
        }
    }
}